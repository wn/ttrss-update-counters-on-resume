# `Update Counters on Resume` Tiny Tiny RSS Plugin

## Overview
This plugin triggers a refresh of counters if it seems the system state has been resumed
(meaning you'll see updated numbers sooner).

## Installation
1. Clone the repo to **prefs_effective_config** in your tt-rss **plugins.local** directory:

   `git clone https://gitlab.tt-rss.org/wn/ttrss-update-counters-on-resume.git update_counters_on_resume`

2. Enable the plugin @ Preferences → Plugins

