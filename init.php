<?php
class Update_Counters_On_Resume extends Plugin {
	public function about() {
		return [
			null, // version
			'Force a counters refresh if it seems the system state has been resumed', // description
			'wn', // author
			false, // is system
			'', // more info URL
		];
	}

	public function api_version() {
		return 2;
	}

	public function init($host): void {
	}

	public function get_js() {
		return <<<'JS'
require(['dojo/ready'], (ready) => {
	ready(() => {
		let last_seen_time = Date.now();

		(function loop() {
			setTimeout(() => {
				if (Math.abs(Date.now() - last_seen_time) > 60*1000) {
					Feeds.requestCounters();
					if (typeof Feeds.getActive() !== 'undefined') {
						Feeds.open({feed: Feeds.getActive(), is_cat: Feeds.activeIsCat()});
					}
				}

				last_seen_time = Date.now();

				loop();
			}, 2000);
		})();
	});
});
JS;
	}
}
